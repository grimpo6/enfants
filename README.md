Dans cet espace, un ensemble de Documents relatifs à l'activité du pôle enfants :

- [des tickets pour échanger](https://gitlab.com/grimpo6/enfants/issues)
- un répertoire pour stocker [le passeport enfants de Roc 14](https://gitlab.com/grimpo6/enfants/-/tree/master/passeport_enfants_roc14)
- un répertoire pour travailler sur [le passeport enfants de Grimpo6](https://gitlab.com/grimpo6/enfants/-/tree/master/passeport_enfants_grimpo6)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

