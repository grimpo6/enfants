Ce répertoire contient le passeport d'escalade pour enfants du club :

- [le PDF](GRIMPO6_PASSEPORT_enfants.pdf) est la version courante imprimable (format A5) du passeport
- [le fichier .sla](GRIMPO6_PASSEPORT_enfants.sla) est le fichier source éditable avec [Scribus](https://www.scribus.net/)
- les images utilisées sont dans [le répertoire img](img)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.